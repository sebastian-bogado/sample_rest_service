package com.bogado.sebastian.cnfg;

import com.bogado.sebastian.rest.EndpointInterface;
import com.bogado.sebastian.rest.PuntajeEndPoint;
import com.bogado.sebastian.rest.UserEndPoint;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by sbogado on 13/02/2017.
 */
@Component
public class JerseyConfig extends ResourceConfig {

		@Autowired
    public JerseyConfig(List<EndpointInterface> endpoints) {
        endpoints.forEach(element -> register(element));
    }

}
