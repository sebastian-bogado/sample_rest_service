package com.bogado.sebastian.rest;

import com.bogado.sebastian.core.model.Puntaje;
import com.bogado.sebastian.core.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import java.util.List;

/**
 * Created by sbogado on 13/02/2017.
 */
@Component
@Path("/puntajes")
public class PuntajeEndPoint implements EndpointInterface {

    @Autowired
    private UserService userService;

    @GET
    @Produces(value = MediaType.APPLICATION_JSON_VALUE)
    public String sarasa() {
        return "Sarasa";
    }


    @GET
    @Path("/getPuntajeByUserId")
    @Produces(value = MediaType.APPLICATION_JSON_VALUE)
    public List<Puntaje> getPuntajeByUserId(@QueryParam("id") @NotNull Integer idUser){
        return userService.findById(idUser).getPuntajes();
    }

}
