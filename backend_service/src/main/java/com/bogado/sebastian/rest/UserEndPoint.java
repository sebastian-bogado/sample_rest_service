package com.bogado.sebastian.rest;

import com.bogado.sebastian.core.model.User;
import com.bogado.sebastian.core.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import java.util.Collection;

/**
 * Created by sbogado on 13/02/2017.
 */
@Component
@Path("/users")
public class UserEndPoint implements EndpointInterface {
    @Autowired
    private UserService userService;

    public UserEndPoint() {
        System.out.println("Me debes una cerveza");
    }

    @GET
    @Produces(value = MediaType.APPLICATION_JSON_VALUE)
    public Collection<User> getAllUsers() {
        return userService.findAll();
    }

    @GET
    @Path("/{id}")
    @Produces(value = MediaType.APPLICATION_JSON_VALUE)
    public User getPuntajeByUserId(@PathParam("id") @NotNull Integer idUser){
        return userService.findById(idUser);
    }

}
