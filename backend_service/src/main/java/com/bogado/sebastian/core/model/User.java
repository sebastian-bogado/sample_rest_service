package com.bogado.sebastian.core.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by sbogado on 13/02/2017.
 */
@Entity
public class User {

    @Id
    @GeneratedValue
    private Integer id;

    @NotNull
    private String username;

    @OneToMany(fetch = FetchType.EAGER,  mappedBy = "user")
    private List<Puntaje> puntajes;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    public List<Puntaje> getPuntajes() {
        return puntajes;
    }

    public void setPuntajes(List<Puntaje> puntajes) {
        this.puntajes = puntajes;
    }
}
