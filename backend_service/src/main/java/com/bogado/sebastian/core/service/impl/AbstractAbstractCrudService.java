package com.bogado.sebastian.core.service.impl;

import com.bogado.sebastian.core.service.AbstractCrudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;

import java.io.Serializable;
import java.util.Collection;

/**
 * Created by sbogado on 13/02/2017.
 */
public class AbstractAbstractCrudService<E, PK extends Serializable> implements AbstractCrudService<E,PK> {
    @Autowired
    private JpaRepository<E, PK> dao;

    protected JpaRepository<E, PK> getDAO() {
        return dao;
    }

    @Override
    public E save(E entity) {
        return getDAO().save(entity);
    }

    @Override
    public E update(E entity) {
        return getDAO().save(entity);
    }

    @Override
    public void delete(E entity) {
        getDAO().delete(entity);
    }

    @Override
    public void deleteById(PK id) {
        getDAO().deleteById(id);
    }

    @Override
    public E findById(PK id) {
        return getDAO().findById(id).get();
    }

    @Override
    public Collection<E> findAll() {
        return getDAO().findAll();
    }

    @Override
    public long count() {
        return getDAO().count();
    }
}
