package com.bogado.sebastian.core.service;

import com.bogado.sebastian.core.model.User;

/**
 * Created by sbogado on 13/02/2017.
 */
public interface UserService extends AbstractCrudService<User, Integer> {
}
