package com.bogado.sebastian.core.service.impl;

import com.bogado.sebastian.core.service.UserService;
import com.bogado.sebastian.core.model.User;
import org.springframework.stereotype.Service;

/**
 * Created by sbogado on 13/02/2017.
 */
@Service
public class UserServiceImpl extends AbstractAbstractCrudService<User, Integer> implements UserService {
}
