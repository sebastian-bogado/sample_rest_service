package com.bogado.sebastian.core.dao;

import com.bogado.sebastian.core.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by sbogado on 13/02/2017.
 */
@Repository
public interface UserDao extends JpaRepository<User, Integer> {
}
