package com.bogado.sebastian.core.service;

import java.io.Serializable;
import java.util.Collection;

/**
 * Created by sbogado on 13/02/2017.
 */
public interface AbstractCrudService<E, PK extends Serializable> {

    E save(E entity);

    E update(E entity);

    void delete(E entity);

    void deleteById(PK id);

    E findById(PK id);

    Collection<E> findAll();

    long count();
}
